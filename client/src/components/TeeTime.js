import React, { useEffect, useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Section,
	Image,
	Dropdown,
	box
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import { parseISO, format } from "date-fns";
import { Moment } from "react-moment";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

//queries
import { getTeeTimesQuery, getTeeTimeQuery } from "../queries/queries";

//import
import {
	createTeeTimeMutation,
	deleteTeeTimeMutation
} from "../queries/mutations.js";

const TeeTime = props => {
	console.log(props);

	//hooks
	const [open, setOpen] = useState(false);
	const [date, setDate] = useState("");
	const [price, setPrice] = useState("");
	const [time, setTime] = useState("");
	const [holes, setHoles] = useState("");
	const [players, setPlayers] = useState("");
	const role = localStorage.getItem("role");

	useEffect(() => {
		console.log("val of date: " + date);
		console.log("val of price: " + price);
		console.log("val of time: " + time);
		console.log("val of holes: " + holes);
		console.log("val of players: " + players);
	});

	const dateChangeHandler = e => {
		// console.log(e.target.value);
		setDate(e.target.value);
	};

	const priceChangeHandler = e => {
		// console.log(e.target.value);
		setPrice(e.target.value);
	};

	const timeChangeHandler = e => {
		// console.log(e.target.value);
		setTime(e.target.value);
	};

	const holesChangeHandler = e => {
		// console.log(e.target.value);
		setHoles(e.target.value);
	};

	const playersChangeHandler = e => {
		console.log(e.target.value);
		setPlayers(e.target.value);
	};

	const deleteTeeTimeHandler = e => {
		// console.log(e.target.id)
		let id = e.target.id;

		Swal.fire({
			title: "Are you sure?",
			text: "You won't be able to revert this!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete the tee time!"
		}).then(result => {
			if (result.value) {
				props.deleteTeeTimeMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getTeeTimesQuery
						}
					]
				});

				Swal.fire("Deleted!", "Tee time has been deleted.", "success");
			}
		});
	};

	// console.log(props);

	const addTeeTime = e => {
		e.preventDefault();
		let newTeeTime = {
			date: date,
			price: price,
			time: time,
			holes: holes,
			players: players
		};

		console.log(newTeeTime);
		props.createTeeTimeMutation({
			variables: newTeeTime,
			refetchQueries: [
				{
					query: getTeeTimesQuery
				}
			]
		});

		//swal
		Swal.fire({
			icon: "success",
			title: "Successfully added a new tee time"
		});

		//clear input boxes
		setDate("");
		setPrice("");
		setTime("");
		setHoles("");
		setPlayers("");
	};

	const teeTimeData = props.getTeeTimesQuery.getTeeTimes
		? props.getTeeTimesQuery.getTeeTimes
		: [];

	if (props.getTeeTimesQuery.loading) {
		let timerInterval;
		Swal.fire({
			title: "Fetching tee times....",
			timer: 1000,
			timerProgressBar: true,
			onBeforeOpen: () => {
				Swal.showLoading();
			},
			onClose: () => {
				clearInterval(timerInterval);
			}
		}).then(result => {
			if (
				/* Read more about handling dismissals below */
				result.dismiss === Swal.DismissReason.timer
			) {
				console.log("I was closed by the timer"); // eslint-disable-line
			}
		});
	}

	if (role === "admin") {
		return (
			<Container className="column">
				<Columns>
					<Columns.Column size={3}>
						<Card id="card1">
							<Card.Header>
								<Card.Header.Title
									id="title"
									className="is-centered"
								>
									Add Tee Time
								</Card.Header.Title>
							</Card.Header>
							<Card.Content>
								<form onSubmit={addTeeTime}>
									<div className="field">
										<label
											className="label is-size-7"
											htmlFor="date"
										>
											Date
										</label>
										<input
											id="date"
											className="input"
											type="date"
											onChange={dateChangeHandler}
											value={date}
										/>
									</div>
									{/*end of field*/}
									<div className="field">
										<label
											className="label is-size-7"
											htmlFor="price"
										>
											Price
										</label>
										<input
											id="price"
											className="input"
											type="text"
											onChange={priceChangeHandler}
											value={price}
										/>
									</div>
									{/*end of field*/}

									<div className="field">
										<label
											className="label is-size-7"
											htmlFor="time"
										>
											Time
										</label>
										<input
											id="time"
											className="input"
											type="text"
											onChange={timeChangeHandler}
											value={time}
										/>
									</div>
									{/*end of field*/}

									<div className="field">
										<label
											className="label is-size-7"
											htmlFor="holes"
										>
											No. of Holes
										</label>
										<input
											id="holes"
											className="input"
											type="text"
											onChange={holesChangeHandler}
											value={holes}
										/>
									</div>
									{/*end of field*/}

									<div className="field">
										<label
											className="label is-size-7"
											htmlFor="players"
										>
											No. of Players
										</label>
										<input
											id="players"
											className="input"
											type="text"
											onChange={playersChangeHandler}
											value={players}
										/>
									</div>
									{/*end of field*/}
									<Button
										type="submit"
										color="success"
										fullwidth
										className="button fas fa-plus-circle is-rounded is-small"
									></Button>
								</form>
							</Card.Content>
						</Card>
					</Columns.Column>

					<Columns.Column size={9}>
						<Card id="card2">
							<Card.Header>
								<Card.Header.Title
									className="is-centered"
									id="title"
								>
									Today's Top Deals
								</Card.Header.Title>
							</Card.Header>
							<Card.Content id="card-content">
								{/*sorter*/}

								{/*end of sorter*/}

								<div
									id="card-content"
									className="columns is-multiline has-text-centered"
								>
									{teeTimeData.map(tee => {
										return (
											<div
												id="test"
												class="column is-one-quarter"
											>
												<div class="card">
													<header className="card-header">
														<p
															id="price"
															className="card-header-title is-left is-size-4"
														>
															&#8369;{tee.price}
														</p>
														<p
															className="is-size-7"
															id="sub"
														>
															per player
														</p>
														<Button
															id={tee.id}
															onClick={
																deleteTeeTimeHandler
															}
															color="dark"
															className="button fas fa-trash-alt btns2 delete "
														></Button>
													</header>
													<header className="card-header">
														<p
															id="price"
															className="card-header-title is-centered"
														>
															{tee.time}
														</p>
													</header>

													<div className="card-content has-text-left">
														<div className="content is-size-7">
															<p id="txt">
																<strong>
																	Date:
																</strong>
																{"  "}
															</p>
															<p>
																{format(
																	parseISO(
																		tee.date
																	),
																	" E.,  d MMM yyyy"
																)}
															</p>
															<p id="txt">
																<strong>
																	Holes:
																</strong>
																{"  "}
																{tee.holes}
															</p>
															<p id="txt">
																<strong>
																	Players:
																</strong>
																{"  "}
																{tee.players}
															</p>

															<Image
																id="img"
																src="/images/logo3.png"
																className="is-32x32 img"
															/>

															<Image
																id="img"
																src="/images/golfcart.jpg"
																className="is-32x32 img"
															/>
														</div>
													</div>
													<footer className="card-footer">
														<div className="btn-group font">
															<Link
																to={
																	"teetime/update/" +
																	tee.id
																}
															>
																<Button
																	id="edit"
																	className="button is-rounded fas fa-edit is-dark is-outlined is-small"
																></Button>
															</Link>
														</div>
													</footer>
												</div>
											</div>
										);
									})}
								</div>
							</Card.Content>
						</Card>
					</Columns.Column>
				</Columns>
			</Container>
		);
	} else if (role === "user") {
		return (
			<Container className="column">
				<Columns>
					<Columns.Column size={10} offset={1}>
						<Card id="card2">
							<Card.Header>
								<Card.Header.Title
									className="is-centered"
									id="title"
								>
									Today's Top Deals
								</Card.Header.Title>
							</Card.Header>
							<Card.Content id="card-content">
								{/*sorter*/}

								{/*end of sorter*/}

								<div
									id="card-content"
									className="columns is-multiline has-text-centered"
								>
									{teeTimeData.map(tee => {
										return (
											<div
												id="test"
												class="column is-one-quarter"
											>
												<div class="card">
													<header className="card-header">
														<div>
															<p
																id="price"
																className="card-header-title is-left is-size-4"
															>
																&#8369;
																{tee.price}
															</p>
														</div>
														<p
															className="is-size-7"
															id="sub2"
														>
															per player
														</p>
													</header>
													<header className="card-header">
														<p
															id="price"
															className="card-header-title is-centered"
														>
															{tee.time}
														</p>
													</header>

													<div className="card-content has-text-left">
														<div className="content is-size-7">
															<p id="txt">
																<strong>
																	Date:
																</strong>
																{"  "}
															</p>
															<p>
																{format(
																	parseISO(
																		tee.date
																	),
																	" E., MMM. d, yyyy"
																)}
															</p>
															<p id="txt">
																<strong>
																	Holes:
																</strong>
																{"  "}
																{tee.holes}
															</p>
															<p id="txt">
																<strong>
																	Players:
																</strong>
																{"  "}
																{tee.players}
															</p>

															<Image
																id="img"
																src="/images/logo3.png"
																className="is-32x32 img"
															/>

															<Image
																id="img"
																src="/images/golfcart.jpg"
																className="is-32x32 img"
															/>
														</div>
													</div>
													<footer className="card-footer">
														<div className="btn-group font">
															<Link
																to={
																	"booking/create/" +
																	tee.id
																}
															>
																<Button
																	id="view"
																	color="success"
																	className="button is-rounded is-small"
																>
																	View
																</Button>
															</Link>
														</div>
													</footer>
												</div>
											</div>
										);
									})}
								</div>
							</Card.Content>
						</Card>
					</Columns.Column>
				</Columns>
			</Container>
		);
	} else {
		return (
			<Container className="column">
				<Columns>
					<Columns.Column size={10} offset={1}>
						<Card id="card2">
							<Card.Header>
								<Card.Header.Title
									className="is-centered"
									id="title"
								>
									Today's Top Deals
								</Card.Header.Title>
							</Card.Header>
							<Card.Content id="card-content">
								{/*sorter*/}

								{/*end of sorter*/}

								<div
									id="card-content"
									className="columns is-multiline has-text-centered"
								>
									{teeTimeData.map(tee => {
										return (
											<div
												id="test"
												class="column is-one-quarter"
											>
												<div class="card">
													<header className="card-header">
														<div>
															<p
																id="price"
																className="card-header-title is-left is-size-4"
															>
																&#8369;
																{tee.price}
															</p>
														</div>
														<p
															className="is-size-7"
															id="sub2"
														>
															per player
														</p>
													</header>
													<header className="card-header">
														<p
															id="price"
															className="card-header-title is-centered"
														>
															{tee.time}
														</p>
													</header>

													<div className="card-content has-text-left">
														<div className="content is-size-7">
															<p id="txt">
																<strong>
																	Date:
																</strong>
																{"  "}
															</p>
															<p>
																{format(
																	parseISO(
																		tee.date
																	),
																	" E., MMM. d, yyyy"
																)}
															</p>
															<p id="txt">
																<strong>
																	Holes:
																</strong>
																{"  "}
																{tee.holes}
															</p>
															<p id="txt">
																<strong>
																	Players:
																</strong>
																{"  "}
																{tee.players}
															</p>

															<Image
																id="img"
																src="/images/logo3.png"
																className="is-32x32 img"
															/>

															<Image
																id="img"
																src="/images/golfcart.jpg"
																className="is-32x32 img"
															/>
														</div>
													</div>
													<footer className="card-footer">
														<div className="btn-group">
															<p>
																<small>
																	{" "}
																	No account
																	yet?
																</small>{" "}
															</p>
															<Link to="/register">
																<button
																	type="button"
																	className="button is-rounded is-small  is-success"
																>
																	Register
																</button>
															</Link>
														</div>
													</footer>
												</div>
											</div>
										);
									})}
								</div>
							</Card.Content>
						</Card>
					</Columns.Column>
				</Columns>
			</Container>
		);
	}
};

//graphql() lets us bind graphql queries with components
// export default graphql(getMembersQuery)(Member);

export default compose(
	graphql(getTeeTimesQuery, { name: "getTeeTimesQuery" }),
	graphql(createTeeTimeMutation, { name: "createTeeTimeMutation" }),
	graphql(deleteTeeTimeMutation, { name: "deleteTeeTimeMutation" })
)(TeeTime);
