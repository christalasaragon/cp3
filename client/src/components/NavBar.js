import React, { useState } from "react";
import { Navbar, Image, Dropdown } from "react-bulma-components";
import { Link } from "react-router-dom";

const NavBar = props => {
	const [open, setOpen] = useState(false);
	const role = localStorage.getItem("role");

	// let customLink = props.username ? "hi " + props.username : "hi guest";

	//solution ni Sir
	let customLink = "";

	if (!props.username) {
		customLink = (
			<Link
				id="login-btn"
				className="button is-success is-rounded is-small navbar-item font"
				to="/login"
			>
				Log in/Register
			</Link>
		);
	} else {
		customLink = (
			<div
				id="navbar-item2"
				class="navbar-item has-dropdown is-hoverable ba"
			>
				<div className="navbar-item font">
					<span id="navbar-item">
						Welcome back, {props.username}!
					</span>
					&nbsp;
					<i class="fas fa-chevron-down"></i>
				</div>
				<div id="drp" class="navbar-dropdown is-boxed">
					<Link className="navbar-item" to="/logout">
						Log out
					</Link>
				</div>
			</div>
		);
	}

	if (role === "admin") {
		return (
			<Navbar id="navbar" active={open} className="is-fixed-top">
				<Navbar.Brand>
					<Image id="logo" src="/images/swing.png" alt="MNL Golf " />
					<Link
						id="navbar-title"
						to="/"
						className="navbar-item font is-size-4"
					>
						<strong>MNL Golf Club</strong>
					</Link>
					<Navbar.Burger
						className="has-text-dark"
						active={open}
						onClick={() => {
							setOpen(!open);
						}}
					/>
				</Navbar.Brand>

				<Navbar.Menu>
					<Navbar.Container position="end" active={open}>
						<Link
							id="navbar-item"
							className="navbar-item font is-size-6"
							to="/teetimes"
						>
							Tee Times
						</Link>
						<Link
							id="navbar-item"
							className="navbar-item font"
							to="/bookings"
						>
							Bookings
						</Link>
						<Link
							id="navbar-item"
							className="navbar-item font is-size-6"
							to="/users"
						>
							<i class="fas fa-users"></i>
						</Link>

						{customLink}

						{/*{customLink}*/}
					</Navbar.Container>
				</Navbar.Menu>
			</Navbar>
		);
	} else if (role === "user") {
		return (
			<Navbar
				id="navbar"
				active={open}
				className="is-fixed-top is-transparent"
			>
				<Navbar.Brand>
					<Image id="logo" src="/images/swing.png" alt="MNL Golf " />
					<Link
						id="navbar-title"
						to="/"
						className="navbar-item font is-size-4"
					>
						<strong>MNL Golf Club</strong>
					</Link>
					<Navbar.Burger
						className="has-text-dark"
						active={open}
						onClick={() => {
							setOpen(!open);
						}}
					/>
				</Navbar.Brand>

				<Navbar.Menu>
					<Navbar.Container position="end" active={open}>
						<Link
							id="navbar-item"
							className="navbar-item font is-size-6"
							to="/teetimes"
						>
							Tee Times
						</Link>
						<Link
							id="navbar-item"
							className="navbar-item font"
							to="/bookings"
						>
							Bookings
						</Link>

						{customLink}

						{/*{customLink}*/}
					</Navbar.Container>
				</Navbar.Menu>
			</Navbar>
		);
	} else {
		return (
			<Navbar
				id="navbar"
				active={open}
				className="is-fixed-top is-transparent"
			>
				<Navbar.Brand>
					<Image id="logo" src="/images/swing.png" alt="MNL Golf " />
					<Link
						id="navbar-title"
						to="/"
						className="navbar-item font is-size-4"
					>
						<strong>MNL Golf Club</strong>
					</Link>
					<Navbar.Burger
						className="has-text-dark"
						active={open}
						onClick={() => {
							setOpen(!open);
						}}
					/>
				</Navbar.Brand>

				<Navbar.Menu>
					<Navbar.Container position="end" active={open}>
						<Link
							id="navbar-item"
							className="navbar-item font is-size-6"
							to="/teetimes"
						>
							Tee Times
						</Link>

						{customLink}

						{/*{customLink}*/}
					</Navbar.Container>
				</Navbar.Menu>
			</Navbar>
		);
	}
};

export default NavBar;
