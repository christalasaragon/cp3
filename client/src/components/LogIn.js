import React, { useState, useEffect } from "react";
import { Container, Columns, Card } from "react-bulma-components";
import { Redirect } from "react-router-dom";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";

// mutations
import { logInMutation } from "../queries/mutations";

const Login = props => {
	console.log(props);

	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [logInSuccess, setLogInSuccess] = useState(false);

	useEffect(() => {
		console.log("value of password: " + password);
		console.log("value of usernmae: " + username);
	});

	const usernameChangeHandler = e => {
		console.log(e.target.value);
		setUsername(e.target.value);
	};

	const passwordChangeHandler = e => {
		setPassword(e.target.value);
	};

	const submitFormHandler = e => {
		e.preventDefault();
		console.log("submitting the log in form...");
		props
			.logInMutation({
				variables: {
					username: username,
					password: password
				}
			})
			.then(res => {
				console.log(res);

				let data = res.data.logInUser;
				console.log(data);

				if (data === null) {
					// swal telling the user that his credentials are wrong
					Swal.fire({
						title: "Login Failed",
						text: "wrong credentials",
						icon: "error"
					});
				} else {
					// credentials are correct
					// setItem("key", "value")
					console.log("successful");
					localStorage.setItem("username", data.username);
					localStorage.setItem("role", data.role);
					props.updateSession();
					setLogInSuccess(true);
				}
			});
	};

	if (!logInSuccess) {
		console.log("something went wrong...");
	} else {
		console.log("successful login");
		return <Redirect to="/" />;
	}

	return (
		<Container>
			<Columns>
				<Columns.Column size="one-third" offset="one-third">
					<Card className="hed4">
						<Card.Header>
							<Card.Header.Title
								id="title"
								className="is-centered"
							>
								Log in
							</Card.Header.Title>
						</Card.Header>

						<Card.Content>
							<form onSubmit={submitFormHandler}>
								<div id="input-login">
									<label
										className="label is-size-7"
										htmlFor="username "
									>
										Username
									</label>
									<p class="control has-icons-left">
										<input
											id="input-login"
											className="input"
											placeholder="Username"
											type="text"
											value={username}
											onChange={usernameChangeHandler}
										/>
										<span class="icon is-small is-left">
											<i class="fas fa-user"></i>
										</span>
									</p>
									<label
										id="password"
										className="label is-size-7"
										htmlFor="date "
									>
										password
									</label>
									<p class="control has-icons-left">
										<input
											id="input-login"
											className="input "
											placeholder="Password"
											type="password"
											value={password}
											onChange={passwordChangeHandler}
										/>
										<span class="icon is-small is-left">
											<i class="fas fa-lock"></i>
										</span>
									</p>
								</div>
								<div className="btn-group" id="btn-group">
									<p>
										<small> No account?</small>{" "}
									</p>
									<Link to="/register">
										<button
											type="button"
											className="button is-rounded is-small  is-dark is-outlined"
										>
											Register
										</button>
									</Link>
									<button
										type="submit"
										class="button is-rounded is-small is-success"
									>
										<span class="icon is-small">
											<i class="fas fa-check"></i>
										</span>
										<span>Log In</span>
									</button>
								</div>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default graphql(logInMutation, { name: "logInMutation" })(Login);
