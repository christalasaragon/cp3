import React, { useEffect, useState } from "react";
import {
	Section,
	Heading,
	Card,
	Columns,
	Container
} from "react-bulma-components";
import { Link } from "react-router-dom";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import { parseISO, format } from "date-fns";
import moment from "moment";

import Swal from "sweetalert2";
//queries
import { getTeeTimeQuery, getTeeTimesQuery } from "../queries/queries";
//import
import { updateTeeTimeMutation } from "../queries/mutations.js";

const UpdateTeeTime = props => {
	console.log(props);

	// let now = moment();
	// console.log(now);

	useEffect(() => {
		console.log("val of date: " + date);
		console.log("val of price: " + price);
		console.log("val of time: " + time);
		console.log("val of holes: " + holes);
		console.log("val of players: " + players);
	});

	//hooks
	const [date, setDate] = useState("");
	const [price, setPrice] = useState("");
	const [time, setTime] = useState("");
	const [holes, setHoles] = useState("");
	const [players, setPlayers] = useState("");

	let tee = props.getTeeTimeQuery.getTeeTime
		? props.getTeeTimeQuery.getTeeTime
		: {};
	console.log(tee.date);

	if (!props.getTeeTimeQuery.loading) {
		let id = props.id;
		const setDefault = () => {
			// let day = moment(tee.date).format("DD/MM/YYYY");
			console.log("hello");
			setDate(tee.date);
			setPrice(tee.price);
			setTime(tee.time);
			setHoles(tee.holes);
			setPlayers(tee.players);
			console.log(date);
		};
		if (date === "") {
			setDefault();
		}
	}

	const dateChangeHandler = e => {
		// console.log(e.target.value);
		setDate(e.target.value);
	};

	const priceChangeHandler = e => {
		// console.log(e.target.value);
		setPrice(e.target.value);
	};

	const timeChangeHandler = e => {
		// console.log(e.target.value);
		setTime(e.target.value);
	};

	const holesChangeHandler = e => {
		// console.log(e.target.value);
		setHoles(e.target.value);
	};

	const playersChangeHandler = e => {
		console.log(e.target.value);
		setPlayers(e.target.value);
	};

	const formSubmitHandler = e => {
		e.preventDefault();
		let id = props.match.params.id;
		let updatedTee = {
			id: id,
			date: date,
			price: price,
			time: time,
			holes: holes,
			players: players
		};
		console.log(updatedTee);
		props.updateTeeTimeMutation({
			variables: updatedTee
		});

		Swal.fire({
			title: "Tee time updated",
			text: "Tee time has been updated",
			type: "success",
			html:
				'<a href="/teetimes" class="button is-success">Go back to tee times</a>',
			showCancelButton: false,
			showConfirmButton: false
			// confirmButtonText: '<a href="/">Go back to members</a>'
		});
		// window.location.href = "/";
	};

	if (props.getTeeTimeQuery.loading) {
		let timerInterval;
		Swal.fire({
			title: "fetching tee time....",
			timer: 1000,
			timerProgressBar: true,
			onBeforeOpen: () => {
				Swal.showLoading();
			},
			onClose: () => {
				clearInterval(timerInterval);
			}
		}).then(result => {
			if (
				/* Read more about handling dismissals below */
				result.dismiss === Swal.DismissReason.timer
			) {
				console.log("I was closed by the timer"); // eslint-disable-line
			}
		});
	}

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card className="hed">
						<Card.Header>
							<Card.Header.Title
								id="title"
								className="is-centered"
							>
								Update Tee Time
							</Card.Header.Title>
						</Card.Header>
						<Card.Content id="sbtitle">
							<form onSubmit={formSubmitHandler}>
								<div id="lbl2">
									<label
										className="label is-size-7"
										htmlFor="date "
									>
										Date
									</label>
									<input
										type="hidden"
										onChange={dateChangeHandler}
										value={date}
										className="input"
									/>
									<p>{moment(date).format("DD MMM YYYY")}</p>
								</div>

								<div id="lbl">
									<label
										className="label is-size-7"
										htmlFor="time"
									>
										Time
									</label>

									<input
										type="hidden"
										onChange={timeChangeHandler}
										value={time}
										className="input"
									/>
									<p>{time}</p>
								</div>

								<div id="lbl">
									<label
										className="label is-size-7"
										htmlFor="price"
									>
										Price
									</label>
									<input
										type="text"
										onChange={priceChangeHandler}
										value={price}
										className="input"
									/>
								</div>

								<div id="lbl">
									<label
										className="label is-size-7"
										htmlFor="password"
									>
										Holes
									</label>

									<input
										type="text"
										onChange={holesChangeHandler}
										value={holes}
										className="input"
									/>
								</div>

								<div id="lbl">
									<label
										className="label is-size-7"
										htmlFor="password"
									>
										Players
									</label>

									<input
										type="text"
										onChange={playersChangeHandler}
										value={players}
										className="input"
									/>
								</div>

								<div className="btn-group" id="btn-group">
									<Link to="/teetimes">
										<button
											type="button"
											className="button is-rounded is-small  is-dark is-outlined"
										>
											Back
										</button>
									</Link>
									<button
										type="submit"
										class="button is-rounded is-small is-success"
									>
										<span class="icon is-small">
											<i class="fas fa-check"></i>
										</span>
										<span>Save</span>
									</button>
								</div>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getTeeTimesQuery, { name: "getTeeTimesQuery" }),
	graphql(updateTeeTimeMutation, { name: "updateTeeTimeMutation" }),
	graphql(getTeeTimeQuery, {
		options: props => {
			//retrieve the wildcard id param
			console.log(props.match.params.id);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getTeeTimeQuery"
	})
)(UpdateTeeTime);
