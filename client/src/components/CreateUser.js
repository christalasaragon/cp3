import React, { useEffect, useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Image,
	Media
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import { toBase64, nodeServer } from "../function.js";
//queries
import { getUsersQuery } from "../queries/queries";

//import
import { createUserMutation } from "../queries/mutations.js";

const CreateUser = props => {
	console.log(props);

	//hooks
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [role, setRole] = useState("user");
	const [password, setPassword] = useState("");
	const [username, setUsername] = useState("");
	const [email, setEmail] = useState("");
	const [imagePath, setImagePath] = useState("");
	const fileRef = React.createRef();

	useEffect(() => {
		console.log("val of firstName: " + firstName);
		console.log("val of lastName: " + lastName);
		console.log("val of role: " + role);
		console.log("val of password: " + password);
		console.log("val of username: " + username);
		console.log("val of email: " + email);
	});

	const firstNameChangeHandler = e => {
		// console.log(e.target.value);
		setFirstName(e.target.value);
	};

	const lastNameChangeHandler = e => {
		// console.log(e.target.value);
		setLastName(e.target.value);
	};

	const roleChangeHandler = e => {
		// console.log(e.target.value);
		setRole(e.target.value);
	};

	const passwordChangeHandler = e => {
		// console.log(e.target.value);
		setPassword(e.target.value);
	};

	const usernameChangeHandler = e => {
		console.log(e.target.value);
		setUsername(e.target.value);
	};

	const emailChangeHandler = e => {
		console.log(e.target.value);
		setEmail(e.target.value);
	};

	// console.log(props);

	const addUser = e => {
		e.preventDefault();
		let newUser = {
			firstName: firstName,
			lastName: lastName,
			role: role,
			password: password,
			username: username,
			email: email,
			imageLocation: imagePath
		};

		console.log(newUser);
		props.createUserMutation({
			variables: newUser,
			refetchQueries: [
				{
					query: getUsersQuery
				}
			]
		});

		//swal
		Swal.fire({
			title: "Registration successful.",
			text: "Registration is successful.",

			html: '<a href="/login" class="button is-success">Log In</a>',
			showCancelButton: false,
			showConfirmButton: false
		});

		//clear input boxes
		setFirstName("");
		setLastName("");
		setRole("");
		setPassword("");
		setUsername("");
		setEmail("");
	};

	const userData = props.getUsersQuery.getUsers
		? props.getUsersQuery.getUsers
		: [];

	console.log(userData);
	// let loadingMessage =data.loading ? <div className="control is-loading"> fetching members...</div>: "";

	const imagePathHandler = e => {
		toBase64(fileRef.current.files[0]).then(encodedFile => {
			setImagePath(encodedFile);
		});
	};

	return (
		<Container className="is-vcentered column">
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card id="createMem">
						<Card.Header>
							<Card.Header.Title
								id="title"
								className="is-centered"
							>
								Register
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={addUser}>
								<div id="lbl" className="field">
									<label
										className="label is-size-7"
										htmlFor="fname"
									>
										First Name
									</label>
									<input
										id="fName"
										className="input"
										type="text"
										onChange={firstNameChangeHandler}
										value={firstName}
										placeholder="First name"
									/>
								</div>
								{/*end of field*/}
								<div id="lbl" className="field">
									<label
										className="label is-size-7"
										htmlFor="lname"
									>
										Last Name
									</label>
									<input
										id="lName"
										className="input"
										type="text"
										onChange={lastNameChangeHandler}
										value={lastName}
										placeholder="Last name"
									/>
								</div>
								{/*end of field*/}

								<div id="lbl" className="field">
									<label
										className="label is-size-7 "
										htmlFor="email"
									>
										Email
									</label>
									<p class="control has-icons-left">
										<input
											id="email"
											className="input"
											type="text"
											onChange={emailChangeHandler}
											value={email}
											placeholder="Email"
										/>
										<span class="icon is-small is-left">
											<i class="fas fa-envelope"></i>
										</span>
									</p>
								</div>
								{/*end of field*/}

								<div id="lbl" className="field">
									<label
										className="label is-size-7"
										htmlFor="password"
									>
										Password
									</label>
									<p class="control has-icons-left">
										<input
											id="password2"
											className="input"
											type="password"
											onChange={passwordChangeHandler}
											value={password}
											placeholder="Password"
										/>
										<span class="icon is-small is-left">
											<i class="fas fa-lock"></i>
										</span>
									</p>
								</div>
								{/*end of field*/}

								<div id="lbl" className="field">
									<label
										className="label is-size-7"
										htmlFor="username"
									>
										Username
									</label>
									<p class="control has-icons-left">
										<input
											id="username"
											className="input"
											type="text"
											onChange={usernameChangeHandler}
											value={username}
											placeholder="Username"
										/>{" "}
										<span class="icon is-small is-left">
											<i class="fas fa-user"></i>
										</span>
									</p>
								</div>
								{/*end of field*/}

								<div id="lbl" className="field">
									<label
										className="label is-size-7"
										htmlFor="image"
									>
										Add Image
									</label>

									<input
										id="profile"
										className="input"
										type="file"
										onChange={imagePathHandler}
										ref={fileRef}
									/>
								</div>
								{/*end of field*/}

								<div id="lbl" className="field">
									<input
										id="role"
										className="input"
										type="hidden"
										onChange={roleChangeHandler}
										value={role}
									/>
								</div>
								{/*end of field*/}

								<div className="btn-group" id="btn-group">
									<button
										type="submit"
										class="button font is-rounded is-small is-success"
									>
										<span>Register</span>
									</button>
								</div>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
				{/*end of main columns.column*/}
			</Columns>
		</Container>
	);
};

//graphql() lets us bind graphql queries with components
// export default graphql(getMembersQuery)(Member);

export default compose(
	graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(createUserMutation, { name: "createUserMutation" })
)(CreateUser);
