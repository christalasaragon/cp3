import React, { useEffect, useState } from "react";
import {
	Section,
	Heading,
	Card,
	Columns,
	Container,
	Image,
	Media,
	Button
} from "react-bulma-components";
import { Link } from "react-router-dom";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import { parseISO, format } from "date-fns";

import Swal from "sweetalert2";
//queries
import {
	getTeeTimeQuery,
	getTeeTimesQuery,
	getBookingsQuery,
	getUsersQuery
} from "../queries/queries";
//import
import { createBookingMutation } from "../queries/mutations.js";

const CreateBooking = props => {
	useEffect(() => {
		console.log("val of date: " + date);
		console.log("val of price: " + price);
		console.log("val of time: " + time);
		console.log("val of holes: " + holes);
	});

	//hooks
	const [date, setDate] = useState("");
	const [price, setPrice] = useState("");
	const [time, setTime] = useState("");
	const [holes, setHoles] = useState("");
	const [players, setPlayers] = useState("");
	const [status, setStatus] = useState("pending");
	const [teeTimeId, setTeeTimeId] = useState("");
	const [username, setUsername] = useState(localStorage.getItem("username"));

	let tee = props.getTeeTimeQuery.getTeeTime
		? props.getTeeTimeQuery.getTeeTime
		: {};
	console.log(tee.id);

	if (!props.getTeeTimeQuery.loading) {
		let id = props.id;
		const setDefault = () => {
			console.log("hello");
			setDate(parseISO(tee.date));
			setPrice(tee.price);
			setPlayers(tee.players);
			setTime(tee.time);
			setHoles(tee.holes);
			setTeeTimeId(tee.id);
			setUsername(localStorage.getItem("username"));
		};
		if (date === "") {
			setDefault();
		}
	}

	const playersChangeHandler = e => {
		console.log(e.target.value);
		setPlayers(e.target.value);
	};

	const statusChangeHandler = e => {
		console.log(e.target.value);
		setStatus(e.target.value);
	};

	const teeTimeIdChangeHandler = e => {
		console.log(e.target.value);
		setTeeTimeId(e.target.value);
	};

	const usernameChangeHandler = e => {
		console.log(e.target.value);
		setUsername(localStorage.getItem("username"));
	};

	const addBooking = e => {
		e.preventDefault();
		let newBooking = {
			players: players,
			status: status,
			teeTimeId: teeTimeId,
			username: username
		};

		console.log(newBooking);
		props.createBookingMutation({
			variables: newBooking,
			refetchQueries: [
				{
					query: getBookingsQuery
				}
			]
		});

		//swal
		Swal.fire({
			title: "Tee time has been added to bookings.",
			text: "Added to bookings",

			html:
				'<a href="/bookings" class="button is-success">View bookings</a>',
			showCancelButton: false,
			showConfirmButton: false
		});
	};

	return (
		<Container id="create">
			<Columns>
				<Columns.Column
					size="one-third"
					offset="one-third"
					className="hed2"
				>
					<Card>
						<Card.Header>
							<Card.Header.Title id="title" className="is-left">
								Tee Time Details
							</Card.Header.Title>
							<Link to="/teetimes">
								<Button
									color="dark"
									className="button fas fa-trash-alt btns2 delete "
								></Button>
							</Link>
						</Card.Header>
						<Card.Content>
							<form onSubmit={addBooking}>
								<div class="media">
									<div class="media-content has-text-centered">
										<p id="title" class="title is-6">
											MNL Golf Club
										</p>
									</div>
								</div>

								<div class="media">
									<div class="media-content has-text-left">
										<p
											id="time"
											className="subtitle is-7 is-uppercase"
										>
											<strong>Tee Time:</strong>{" "}
											{tee.time}
										</p>
										<p
											id="holes"
											class="subtitle is-7 is-uppercase"
										>
											<strong>Holes:</strong> {tee.holes}
										</p>
										<p
											id="holes"
											class="subtitle is-7 is-uppercase"
										>
											<strong>No. of Players:</strong>{" "}
											{tee.players}
										</p>
										<p class="subtitle is-7 is-uppercase">
											<strong>Green Fee:</strong> &#8369;
											{tee.price} per player
										</p>
										<ul>
											<li
												id="holes"
												className="subtitle is-7 is-uppercase"
											>
												<strong>Also includes:</strong>
											</li>

											<li
												id="holes"
												class="subtitle is-7"
											>
												<i class="fas fa-check"></i>{" "}
												Golf cart with GPS
											</li>
											<li
												id="holes"
												class="subtitle is-7"
											>
												<i class="fas fa-check"></i>{" "}
												Scorecard and tees
											</li>
											<li class="subtitle is-7">
												<i class="fas fa-check"></i>{" "}
												Driving range access
											</li>
										</ul>

										<ul
											id="cp"
											className="has-text-centered"
										>
											<li
												id="holes"
												class="subtitle is-7 is-uppercase"
											>
												<strong>
													Tee time deposit required
												</strong>
											</li>
											<li class="subtitle is-7 is-uppercase">
												<strong>Cancel policy:</strong>
												48 hrs.
											</li>
										</ul>
									</div>
								</div>

								<hr id="hr" />

								<div id="lbl">
									<input
										type="hidden"
										onChange={playersChangeHandler}
										value={players}
										className="input is-small"
										placeholder="No. of Players"
									/>
								</div>

								<div id="lbl">
									<input
										type="hidden"
										onChange={usernameChangeHandler}
										value={username}
										className="input is-small"
										placeholder="User Id"
									/>
								</div>

								<div>
									<input
										type="hidden"
										id="inputPlayers"
										onChange={statusChangeHandler}
										value={status}
										className="input is-small is-right"
										placeholder="Status"
									/>

									<input
										type="hidden"
										onChange={teeTimeIdChangeHandler}
										value={teeTimeId}
										className="input is-small is-fullwidth"
										placeholder="teeTimeId"
									/>
								</div>

								<div id="addbtn">
									<button
										id="add"
										type="submit"
										class="button is-success is-rounded is-small"
									>
										Book
									</button>
								</div>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getTeeTimesQuery, { name: "getTeeTimesQuery" }),
	graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(getBookingsQuery, { name: "getBookingsQuery" }),
	graphql(createBookingMutation, { name: "createBookingMutation" }),
	graphql(getTeeTimeQuery, {
		options: props => {
			//retrieve the wildcard id param
			console.log(props.match.params.id);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getTeeTimeQuery"
	})
)(CreateBooking);
