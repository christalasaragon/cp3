import React, { useEffect, useState } from "react";
import {
	Section,
	Heading,
	Card,
	Columns,
	Container
} from "react-bulma-components";
import { Link } from "react-router-dom";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import { parseISO } from "date-fns";

import Swal from "sweetalert2";
//queries
import { getBookingQuery, getBookingsQuery } from "../queries/queries";
//import
import { updateBookingMutation } from "../queries/mutations.js";

const UpdateBooking = props => {
	console.log(props);

	useEffect(() => {
		console.log("val of username: " + username);
		console.log("val of status: " + status);
		console.log("val of players: " + players);
		console.log("val of teeTimeId: " + teeTimeId);
	});

	//hooks
	const [players, setPlayers] = useState("");
	const [status, setStatus] = useState("");
	const [username, setUsername] = useState("");
	const [teeTimeId, setTeeTimeId] = useState("");

	let booking = props.getBookingQuery.getBooking
		? props.getBookingQuery.getBooking
		: {};
	console.log(booking.status);

	if (!props.getBookingQuery.loading) {
		let id = props.id;
		const setDefault = () => {
			console.log("hello");

			setPlayers(booking.players);
			setStatus(booking.status);
			setTeeTimeId(booking.teeTimeId);
			setUsername(booking.username);
		};
		if (username === "") {
			setDefault();
		}
	}

	const usernameChangeHandler = e => {
		// console.log(e.target.value);
		setUsername(e.target.value);
	};

	const teeTimeIdChangeHandler = e => {
		// console.log(e.target.value);
		setTeeTimeId(e.target.value);
	};

	const statusChangeHandler = e => {
		// console.log(e.target.value);
		setStatus(e.target.value);
	};

	const playersChangeHandler = e => {
		// console.log(e.target.value);
		setPlayers(e.target.value);
	};

	const formSubmitHandler = e => {
		e.preventDefault();
		let id = props.match.params.id;
		let updatedBooking = {
			id: id,
			username: username,
			status: status,
			players: players,
			teeTimeId: teeTimeId
		};
		console.log(updatedBooking);
		props.updateBookingMutation({
			variables: updatedBooking
		});

		Swal.fire({
			title: "Booking has been updated",
			text: "Booking has been updated",
			type: "success",
			html:
				'<a href="/bookings" class="button is-success">Go back to bookings</a>',
			showCancelButton: false,
			showConfirmButton: false
			// confirmButtonText: '<a href="/">Go back to members</a>'
		});
		// window.location.href = "/";
	};

	return (
		<Container>
			<Columns>
				<Columns.Column size="one-third" offset="one-third">
					<Heading className="has-text-centered hed3"></Heading>
					<Card>
						<Card.Header>
							<Card.Header.Title
								id="title"
								className="is-centered"
							>
								Booking Status
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
								<input
									type="hidden"
									onChange={usernameChangeHandler}
									value={username}
									className="input"
								/>
								<input
									type="hidden"
									onChange={teeTimeIdChangeHandler}
									value={teeTimeId}
									className="input"
								/>

								<div id="lbl2">
									<label
										className="label is-size-7"
										htmlFor="status"
									>
										Status
									</label>

									<input
										type="text"
										onChange={statusChangeHandler}
										value={status}
										className="input"
									/>
								</div>

								<div id="lbl">
									<input
										type="hidden"
										onChange={playersChangeHandler}
										value={players}
										className="input"
									/>
								</div>

								<div className="btn-group" id="btn-group2">
									<Link to="/bookings">
										<button
											type="button"
											className="button is-small is-rounded is-dark is-outlined"
										>
											Back
										</button>
									</Link>
									<button
										type="submit"
										className="button is-success is-small is-rounded"
									>
										<span className="icon is-small">
											<i class="fas fa-check"></i>
										</span>
										<span>Save</span>
									</button>
								</div>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getBookingsQuery, { name: "getBookingsQuery" }),
	graphql(updateBookingMutation, { name: "updateBookingMutation" }),
	graphql(getBookingQuery, {
		options: props => {
			//retrieve the wildcard id param
			console.log(props.match.params.id);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getBookingQuery"
	})
)(UpdateBooking);
