import React from "react";
import {
	Container,
	Columns,
	Button,
	Section,
	Dropdown,
	Hero,
	Box
} from "react-bulma-components";
import { Link } from "react-router-dom";

const Landing = props => {
	return (
		<div
			id="hero"
			className="hero-body "
			style={{
				backgroundImage: "url(" + "/images/royal-golf-club.jpg" + ")",
				backgroundPosition: "center top",
				backgroundSize: "cover",
				backgroundRepeat: "no-repeat",
				height: "100vh"
			}}
		>
			<div id="hero2" class="container has-text-centered">
				<div className="column is-8 is-offset-2 ">
					<h1 className="title has-text-white is-size-2">
						Unrivalled course. Unrivalled services.
					</h1>
					<h2 id="subs" className="subtitle has-text-white is-size-5">
						Save up to 50% on green fee by booking your tee time
						online!
					</h2>

					<Link to="/teetimes">
						<button
							type="button"
							className="button font is-dark is-rounded is-small	 "
						>
							Book now
						</button>
					</Link>
				</div>
			</div>
		</div>
	);
};

export default Landing;
