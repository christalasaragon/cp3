import React, { useEffect, useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Image,
	Media
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import { toBase64, nodeServer } from "../function.js";
//queries
import { getUsersQuery } from "../queries/queries";

//import
import {
	createUserMutation,
	deleteUserMutation
} from "../queries/mutations.js";
// import {deleteMemberMutation} from "../queries/mutations.js"

const User = props => {
	console.log(props);

	//hooks
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [role, setRole] = useState("user");
	const [password, setPassword] = useState("");
	const [username, setUsername] = useState("");
	const [email, setEmail] = useState("");
	const [imagePath, setImagePath] = useState("");
	const fileRef = React.createRef();

	useEffect(() => {
		console.log("val of firstName: " + firstName);
		console.log("val of lastName: " + lastName);
		console.log("val of role: " + role);
		console.log("val of password: " + password);
		console.log("val of username: " + username);
		console.log("val of email: " + email);
	});

	const firstNameChangeHandler = e => {
		// console.log(e.target.value);
		setFirstName(e.target.value);
	};

	const lastNameChangeHandler = e => {
		// console.log(e.target.value);
		setLastName(e.target.value);
	};

	const roleChangeHandler = e => {
		// console.log(e.target.value);
		setRole(e.target.value);
	};

	const passwordChangeHandler = e => {
		// console.log(e.target.value);
		setPassword(e.target.value);
	};

	const usernameChangeHandler = e => {
		console.log(e.target.value);
		setUsername(e.target.value);
	};

	const emailChangeHandler = e => {
		console.log(e.target.value);
		setEmail(e.target.value);
	};

	const deleteUserHandler = e => {
		// console.log(e.target.id)
		let id = e.target.id;

		Swal.fire({
			title: "Are you sure?",
			text: "You won't be able to revert this!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes!"
		}).then(result => {
			if (result.value) {
				props.deleteUserMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getUsersQuery
						}
					]
				});

				Swal.fire("Deleted!", "User has been deleted.", "success");
			}
		});
	};

	// console.log(props);

	const addUser = e => {
		e.preventDefault();
		let newUser = {
			firstName: firstName,
			lastName: lastName,
			role: role,
			password: password,
			username: username,
			email: email,
			imageLocation: imagePath
		};

		console.log(newUser);
		props.createUserMutation({
			variables: newUser,
			refetchQueries: [
				{
					query: getUsersQuery
				}
			]
		});

		//swal
		Swal.fire({
			icon: "success",
			title: "Successfully added a new user"
		});

		//clear input boxes
		setFirstName("");
		setLastName("");
		setRole("");
		setPassword("");
		setUsername("");
		setEmail("");
	};

	const userData = props.getUsersQuery.getUsers
		? props.getUsersQuery.getUsers
		: [];

	console.log(userData);
	// let loadingMessage =data.loading ? <div className="control is-loading"> fetching members...</div>: "";
	const imagePathHandler = e => {
		toBase64(fileRef.current.files[0]).then(encodedFile => {
			setImagePath(encodedFile);
		});
	};
	if (props.getUsersQuery.loading) {
		let timerInterval;
		Swal.fire({
			title: "fetching users....",
			timer: 1000,
			timerProgressBar: true,
			onBeforeOpen: () => {
				Swal.showLoading();
			},
			onClose: () => {
				clearInterval(timerInterval);
			}
		}).then(result => {
			if (
				/* Read more about handling dismissals below */
				result.dismiss === Swal.DismissReason.timer
			) {
				console.log("I was closed by the timer"); // eslint-disable-line
			}
		});
	}

	return (
		<Container className="is-vcentered column">
			<Columns>
				<Columns.Column size={3}>
					<Card id="card1">
						<Card.Header>
							<Card.Header.Title
								id="title"
								className="is-centered"
							>
								Add member
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={addUser}>
								<div className="field">
									<label
										className="label is-size-7"
										htmlFor="fname"
									>
										First Name
									</label>
									<input
										id="fName"
										className="input"
										type="text"
										onChange={firstNameChangeHandler}
										value={firstName}
									/>
								</div>
								{/*end of field*/}
								<div className="field">
									<label
										className="label is-size-7"
										htmlFor="lname"
									>
										Last Name
									</label>
									<input
										id="lName"
										className="input"
										type="text"
										onChange={lastNameChangeHandler}
										value={lastName}
									/>
								</div>
								{/*end of field*/}

								<div className="field">
									<input
										id="role"
										className="input"
										type="hidden"
										onChange={roleChangeHandler}
										value={role}
									/>
								</div>
								{/*end of field*/}

								<div className="field">
									<label
										className="label is-size-7"
										htmlFor="username"
									>
										Username
									</label>
									<input
										id="username"
										className="input"
										type="text"
										onChange={usernameChangeHandler}
										value={username}
									/>
								</div>
								{/*end of field*/}

								<div className="field">
									<label
										className="label is-size-7"
										htmlFor="password"
									>
										Password
									</label>
									<input
										id="password"
										className="input"
										type="password"
										onChange={passwordChangeHandler}
										value={password}
									/>
								</div>
								{/*end of field*/}

								<div className="field">
									<label
										className="label is-size-7"
										htmlFor="email"
									>
										Email
									</label>
									<input
										id="email"
										className="input"
										type="text"
										onChange={emailChangeHandler}
										value={email}
									/>
								</div>
								{/*end of field*/}

								<div className="field">
									<label
										className="label is-size-7"
										htmlFor="image"
									>
										Add Image
									</label>

									<input
										id="profile"
										className="input"
										type="file"
										onChange={imagePathHandler}
										ref={fileRef}
									/>
								</div>
								{/*end of field*/}

								<Button
									type="submit"
									color="success"
									fullwidth
									className="button fas fa-plus-circle is-rounded"
								></Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>

				<Columns.Column size={9}>
					<Card id="card2">
						<Card.Header>
							<Card.Header.Title
								id="title"
								className="is-centered"
							>
								Club Members
							</Card.Header.Title>
						</Card.Header>
						<div id="card-content" className="columns is-multiline">
							{userData.map(user => {
								return (
									<div id="test" className="column is-half">
										<div className="card">
											<div
												id="imgcard"
												className="card-content has-text-left"
											>
												<div className="content">
													<div class="media">
														<div>
															<Image
																id="image"
																src={
																	nodeServer() +
																	user.imageLocation
																}
																alt="Placeholder image"
																className="is-96x96"
															/>
														</div>

														<div class="media-content">
															<p class="title is-5">
																{user.firstName}{" "}
																{user.lastName}
															</p>
															<p class="subtitle is-6">
																{user.username}
															</p>
															<p
																id="txt"
																className="is-size-7"
															>
																<strong>
																	Role:
																</strong>
																{"  "}
																{user.role}
															</p>
															<p
																id="txt"
																className="is-size-7"
															>
																<strong>
																	Email:
																</strong>
																{"  "}
																{user.email}
															</p>
														</div>
													</div>
												</div>
											</div>
											<footer className="card-footer">
												<div className="btn-group">
													<Button
														id={user.id}
														onClick={
															deleteUserHandler
														}
														color="dark"
														className="button fas fa-trash-alt is-small is-rounded is-outlined"
													></Button>
													<Link
														to={
															"user/update/" +
															user.id
														}
													>
														<Button
															id="edit"
															className="button is-small is-rounded fas fa-user-edit is-dark "
														></Button>
													</Link>

													{/*<Link
														to={
															"/bookings/" +
															user.id
														}
													><Button
															className="btns2 "
															color="dark"
															className="button is-small is-dark is-rounded"
														>
															Bookings
														</Button></Link>*/}
												</div>
											</footer>
										</div>
									</div>
								);
							})}
						</div>
						{/*end of div card-content*/}
					</Card>
					{/*end of main card*/}
				</Columns.Column>
				{/*end of main columns.column*/}
			</Columns>
		</Container>
	);
};

//graphql() lets us bind graphql queries with components
// export default graphql(getMembersQuery)(Member);

export default compose(
	graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(createUserMutation, { name: "createUserMutation" }),
	graphql(deleteUserMutation, { name: "deleteUserMutation" })
)(User);
