import React, { useEffect, useState } from "react";
import {
	Section,
	Heading,
	Card,
	Columns,
	Container,
	Image
} from "react-bulma-components";
import { Link } from "react-router-dom";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import { parseISO } from "date-fns";
import { toBase64, nodeServer } from "../function.js";

import Swal from "sweetalert2";
//queries
import { getUserQuery, getUsersQuery } from "../queries/queries";
//import
import { updateUserMutation } from "../queries/mutations.js";

const UpdateUser = props => {
	console.log(props);

	useEffect(() => {
		console.log("val of firstName: " + firstName);
		console.log("val of lastName: " + lastName);
		console.log("val of role: " + role);
		console.log("val of password: " + password);
		console.log("val of username: " + username);
		console.log("val of email: " + email);
	});

	//hooks
	const [id, setId] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [role, setRole] = useState("");
	const [password, setPassword] = useState("");
	const [username, setUsername] = useState("");
	const [email, setEmail] = useState("");
	const [imagePath, setImagePath] = useState("");
	const fileRef = React.createRef();

	let user = props.getUserQuery.getUser ? props.getUserQuery.getUser : {};
	console.log(user.date);

	if (!props.getUserQuery.loading) {
		const setDefault = () => {
			console.log("hello");
			setId(user.id);
			setFirstName(user.firstName);
			setLastName(user.lastName);
			setRole(user.role);
			setPassword(user.password);
			setUsername(user.username);
			setEmail(user.email);
		};
		if (id === "") {
			setDefault();
		}
	}

	const firstNameChangeHandler = e => {
		// console.log(e.target.value);
		setFirstName(e.target.value);
	};

	const lastNameChangeHandler = e => {
		// console.log(e.target.value);
		setLastName(e.target.value);
	};

	const roleChangeHandler = e => {
		// console.log(e.target.value);
		setRole(e.target.value);
	};

	const passwordChangeHandler = e => {
		// console.log(e.target.value);
		setPassword(e.target.value);
	};

	const usernameChangeHandler = e => {
		console.log(e.target.value);
		setUsername(e.target.value);
	};

	const emailChangeHandler = e => {
		console.log(e.target.value);
		setEmail(e.target.value);
	};

	const formSubmitHandler = e => {
		e.preventDefault();
		let id = props.match.params.id;
		let updatedUser = {
			id: id,
			firstName: firstName,
			lastName: lastName,
			role: role,
			password: password,
			username: username,
			email: email,
			imageLocation: imagePath
		};
		console.log(updatedUser);
		props.updateUserMutation({
			variables: updatedUser
		});

		Swal.fire({
			title: "User updated",
			text: "User has been updated",
			type: "success",
			html:
				'<a href="/users" class="button is-success">Go back to users</a>',
			showCancelButton: false,
			showConfirmButton: false
			// confirmButtonText: '<a href="/">Go back to members</a>'
		});
		// window.location.href = "/";
	};

	const imagePathHandler = e => {
		toBase64(fileRef.current.files[0]).then(encodedFile => {
			setImagePath(encodedFile);
		});
	};

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card className="hed5">
						<Card.Header>
							<Card.Header.Title
								id="title"
								className="is-centered"
							>
								Update User
							</Card.Header.Title>
						</Card.Header>
						<Card.Content id="sbtitle">
							<form onSubmit={formSubmitHandler}>
								<div id="lbl2">
									<label
										className="label is-size-7"
										htmlFor="fname"
									>
										First Name
									</label>
									<input
										id="fName"
										className="input"
										type="text"
										onChange={firstNameChangeHandler}
										value={firstName}
									/>
								</div>

								<div id="lbl">
									<label
										className="label is-size-7"
										htmlFor="lname"
									>
										Last Name
									</label>
									<input
										id="lName"
										className="input"
										type="text"
										onChange={lastNameChangeHandler}
										value={lastName}
									/>
								</div>

								<div id="lbl">
									<label
										className="label is-size-7 "
										htmlFor="email"
									>
										Email
									</label>

									<input
										id="email"
										className="input"
										type="text"
										onChange={emailChangeHandler}
										value={email}
									/>
								</div>

								<div id="lbl">
									<label
										className="label is-size-7"
										htmlFor="username"
									>
										Username
									</label>
									<input
										id="username"
										className="input"
										type="text"
										onChange={usernameChangeHandler}
										value={username}
										placeholder="Username"
									/>
								</div>

								<div id="lbl" className="field">
									<label
										className="label is-size-7"
										htmlFor="image"
									>
										New Image
									</label>
									<input
										id="profile"
										className="input"
										type="file"
										onChange={imagePathHandler}
										ref={fileRef}
									/>
								</div>

								<div id="lbl" className="field">
									<label
										className="label is-size-7"
										htmlFor="role"
									>
										Role
									</label>
									<input
										id="role"
										className="input"
										type="text"
										onChange={roleChangeHandler}
										value={role}
									/>
								</div>

								<div id="lbl">
									<label
										className="label is-size-7"
										htmlFor="role"
									>
										Current Image
									</label>
									<Image
										id="image"
										src={nodeServer() + user.imageLocation}
										alt="Placeholder image"
										className="is-96x96"
									/>
								</div>

								<div id="lbl">
									<input
										id="password2"
										className="input"
										type="hidden"
										onChange={passwordChangeHandler}
										value={password}
									/>
								</div>

								<div className="btn-group" id="btn-group">
									<Link to="/users">
										<button
											type="button"
											className="button is-rounded is-small  is-dark is-outlined"
										>
											Back
										</button>
									</Link>
									<button
										type="submit"
										class="button is-rounded is-small is-success"
									>
										<span class="icon is-small">
											<i class="fas fa-check"></i>
										</span>
										<span>Save</span>
									</button>
								</div>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(updateUserMutation, { name: "updateUserMutation" }),
	graphql(getUserQuery, {
		options: props => {
			//retrieve the wildcard id param
			console.log(props.match.params.id);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getUserQuery"
	})
)(UpdateUser);
