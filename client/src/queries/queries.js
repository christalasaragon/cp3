import { gql } from "apollo-boost";

const getTeeTimesQuery = gql`
	{
		getTeeTimes {
			id
			date
			price
			time
			holes
			players
		}
	}
`;

const getUsersQuery = gql`
	{
		getUsers {
			id
			firstName
			lastName
			role
			password
			username
			email
			imageLocation
		}
	}
`;

const getBookingsQuery = gql`
	{
		getBookings {
			id
			username
			teeTimeId
			status
			players
			users {
				username
				firstName
				lastName
				role
				email
			}
			teetimes {
				date
				time
				price
				holes
			}
		}
	}
`;

const getTeeTimeQuery = gql`
	query($id: ID!) {
		getTeeTime(id: $id) {
			id
			date
			price
			time
			holes
			players
		}
	}
`;

const getUserQuery = gql`
	query($id: ID!) {
		getUser(id: $id) {
			id
			firstName
			lastName
			role
			password
			username
			email
			imageLocation
			bookings {
				teeTimeId
				status
				teetimes {
					date
					price
					time
					holes
					players
				}
			}
		}
	}
`;

const getBookingQuery = gql`
	query($id: ID!) {
		getBooking(id: $id) {
			id
			username
			status
			players
			teeTimeId
		}
	}
`;

export {
	getTeeTimesQuery,
	getUsersQuery,
	getBookingsQuery,
	getTeeTimeQuery,
	getBookingQuery,
	getUserQuery
};
