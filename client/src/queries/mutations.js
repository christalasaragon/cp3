import { gql } from "apollo-boost";

const createTeeTimeMutation = gql`
	mutation(
		$date: Date!
		$price: String!
		$time: String!
		$holes: String!
		$players: String!
	) {
		createTeeTime(
			date: $date
			price: $price
			time: $time
			holes: $holes
			players: $players
		) {
			id
			date
			price
			time
			holes
			players
		}
	}
`;

const createUserMutation = gql`
	mutation(
		$firstName: String!
		$lastName: String!
		$role: String!
		$username: String!
		$password: String!
		$email: String!
		$imageLocation: String
	) {
		createUser(
			firstName: $firstName
			lastName: $lastName
			role: $role
			username: $username
			password: $password
			email: $email
			imageLocation: $imageLocation
		) {
			id
			firstName
			lastName
			role
			username
			password
			email
			imageLocation
		}
	}
`;

const createBookingMutation = gql`
	mutation(
		$username: String!
		$teeTimeId: String!
		$status: String!
		$players: String!
	) {
		createBooking(
			username: $username
			teeTimeId: $teeTimeId
			status: $status
			players: $players
		) {
			id
			username
			teeTimeId
			status
			players
		}
	}
`;

const deleteTeeTimeMutation = gql`
	mutation($id: ID!) {
		deleteTeeTime(id: $id)
	}
`;

const deleteUserMutation = gql`
	mutation($id: ID!) {
		deleteUser(id: $id)
	}
`;

const deleteBookingMutation = gql`
	mutation($id: ID!) {
		deleteBooking(id: $id)
	}
`;

const updateTeeTimeMutation = gql`
	mutation(
		$id: ID!
		$date: Date!
		$price: String!
		$time: String!
		$holes: String!
		$players: String!
	) {
		updateTeeTime(
			id: $id
			date: $date
			price: $price
			time: $time
			holes: $holes
			players: $players
		) {
			id
			date
			price
			time
			holes
			players
		}
	}
`;

const updateUserMutation = gql`
	mutation(
		$id: ID!
		$firstName: String!
		$lastName: String!
		$role: String!
		$username: String!
		$password: String!
		$email: String!
		$imageLocation: String
	) {
		updateUser(
			id: $id
			firstName: $firstName
			lastName: $lastName
			role: $role
			username: $username
			password: $password
			email: $email
			imageLocation: $imageLocation
		) {
			id
			firstName
			lastName
			role
			username
			password
			email
			imageLocation
		}
	}
`;

const updateBookingMutation = gql`
	mutation(
		$id: ID!
		$username: String!
		$status: String!
		$players: String!
		$teeTimeId: String!
	) {
		updateBooking(
			id: $id
			username: $username
			status: $status
			players: $players
			teeTimeId: $teeTimeId
		) {
			id
			username
			status
			players
			teeTimeId
		}
	}
`;

const logInMutation = gql`
	mutation($username: String!, $password: String!) {
		logInUser(username: $username, password: $password) {
			lastName
			firstName
			password
			username
			role
		}
	}
`;

export {
	createTeeTimeMutation,
	createUserMutation,
	createBookingMutation,
	deleteTeeTimeMutation,
	deleteUserMutation,
	deleteBookingMutation,
	updateTeeTimeMutation,
	updateUserMutation,
	updateBookingMutation,
	logInMutation
};
