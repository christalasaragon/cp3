import React, { useState } from "react";
import "./App.css";
import "react-bulma-components/dist/react-bulma-components.min.css";
import { Box } from "react-bulma-components";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import { Moment } from "react-moment";

// components
import User from "./components/User";
import Landing from "./components/Landing";
import TeeTime from "./components/TeeTime";
import Booking from "./components/Booking";
import NavBar from "./components/NavBar";
import LogIn from "./components/LogIn";
import UpdateTeeTime from "./components/UpdateTeeTime";
import UpdateBooking from "./components/UpdateBooking";
import UpdateUser from "./components/UpdateUser";
import CreateBooking from "./components/CreateBooking";
import CreateUser from "./components/CreateUser";

const client = new ApolloClient({ uri: "http://localhost:4000/capstone3" });

function App() {
  const [username, setUserName] = useState(localStorage.getItem("username"));
  const [role, setRole] = useState(localStorage.getItem("role"));
  const [userId, setUserId] = useState(localStorage.getItem("userId"));

  console.log("this is the value of username: " + username);
  console.log("this is the value of username: " + userId);
  console.log("this is the value of position: " + role);

  const updateSession = () => {
    setUserName(localStorage.getItem("username"));
    setUserId(localStorage.getItem("userId"));
    setRole(localStorage.getItem("role"));
  };

  const Logout = () => {
    localStorage.clear();
    updateSession();
    return <Redirect to="/login" />;
  };

  const loggedUser = props => {
    // ... (spread operator) retains all the existing props
    // and added a new prop called updateSession
    return <LogIn {...props} updateSession={updateSession} />;
  };

  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <NavBar username={username} />
        <Switch>
          <Route exact path="/" component={Landing} />
          <Route exact path="/users" component={User} />
          <Route path="/teetimes" component={TeeTime} />
          <Route exact path="/bookings" component={Booking} />
          <Route path="/login" render={loggedUser} />
          <Route path="/teetime/update/:id" component={UpdateTeeTime} />
          <Route path="/booking/create/:id" component={CreateBooking} />
          <Route path="/booking/update/:id" component={UpdateBooking} />

          <Route path="/user/update/:id" component={UpdateUser} />
          <Route path="/logout" component={Logout} />
          <Route path="/register" component={CreateUser} />
        </Switch>
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
