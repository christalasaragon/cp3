<Columns.Column size={3}>
	<Card id="card1">
		<Card.Header>
			<Card.Header.Title id="title" className="is-centered">
				Add user
			</Card.Header.Title>
		</Card.Header>
		<Card.Content>
			<form onSubmit={addUser}>
				<div className="field">
					<label className="label is-size-7" htmlFor="fname">
						First Name
					</label>
					<input
						id="fName"
						className="input"
						type="text"
						onChange={firstNameChangeHandler}
						value={firstName}
					/>
				</div>
				{/*end of field*/}
				<div className="field">
					<label className="label is-size-7" htmlFor="lname">
						Last Name
					</label>
					<input
						id="lName"
						className="input"
						type="text"
						onChange={lastNameChangeHandler}
						value={lastName}
					/>
				</div>
				{/*end of field*/}

				<div className="field">
					<input
						id="role"
						className="input"
						type="hidden"
						onChange={roleChangeHandler}
						value={role}
					/>
				</div>
				{/*end of field*/}

				<div className="field">
					<label className="label is-size-7" htmlFor="username">
						Username
					</label>
					<input
						id="username"
						className="input"
						type="text"
						onChange={usernameChangeHandler}
						value={username}
					/>
				</div>
				{/*end of field*/}

				<div className="field">
					<label className="label is-size-7" htmlFor="password">
						Password
					</label>
					<input
						id="password"
						className="input"
						type="password"
						onChange={passwordChangeHandler}
						value={password}
					/>
				</div>
				{/*end of field*/}

				<div className="field">
					<label className="label is-size-7" htmlFor="email">
						Email
					</label>
					<input
						id="email"
						className="input"
						type="text"
						onChange={emailChangeHandler}
						value={email}
					/>
				</div>
				{/*end of field*/}

				<Button
					type="submit"
					color="success"
					fullwidth
					className="button fas fa-plus-circle is-rounded"
				></Button>
			</form>
		</Card.Content>
	</Card>
</Columns.Column>;
