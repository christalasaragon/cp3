//Always import these two
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const statusSchema = new Schema(
	{
		// list of types:
		// String
		// Number
		// Date
		// Buffer
		// Boolean
		// Mixed
		// ObjectId
		// Array
		// Decimal128
		// Map

		name: {
			type: String,
			required: true
			//lowercase: true // Always convert `test` to lowercase
		}
	},
	{
		timestamps: true
	}
);

// Export Model and import it to your queries.js to use it
module.exports = mongoose.model("Status", statusSchema);
