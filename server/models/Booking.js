//Sample Model

//Always import these two
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const bookingSchema = new Schema(
	{
		// list of types:
		// String
		// Number
		// Date
		// Buffer
		// Boolean
		// Mixed
		// ObjectId
		// Array
		// Decimal128
		// Map

		username: {
			type: String,
			required: true
			//lowercase: true // Always convert `test` to lowercase
		},

		teeTimeId: {
			type: String,
			required: true
		},

		status: {
			type: String,
			required: true
		},

		players: {
			type: String,
			required: true
		}
	},
	{
		timestamps: true
	}
);

// Export Model and import it to your queries.js to use it
module.exports = mongoose.model("Booking", bookingSchema);
