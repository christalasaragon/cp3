//Sample Model

//Always import these two
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const teeTimeSchema = new Schema(
	{
		// list of types:
		// String
		// Number
		// Date
		// Buffer
		// Boolean
		// Mixed
		// ObjectId
		// Array
		// Decimal128
		// Map

		date: {
			type: Date,
			required: true
			//lowercase: true // Always convert `test` to lowercase
		},
		price: {
			type: String,
			required: true
		},
		time: {
			type: String,
			required: true
			//Showtimes: [{ Title: String, date: Date }]
		},
		holes: {
			type: String,
			required: false
		},
		players: {
			type: String,
			required: false
		}
	},
	{
		timestamps: true
	}
);

// Export Model and import it to your queries.js to use it
module.exports = mongoose.model("TeeTime", teeTimeSchema);
