// import/require packages
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
// initialize app
const app = express();
const port = 4000;

//Create a database in your atlas.
//Put the credentials in the code below

//connect to mongoose
// username: b43_merng_db
// Password: N6VSYcNm2zvbOiwH
// Sample:
// mongoose.connect("mongodb+srv://b43_merng_db:N6VSYcNm2zvbOiwH@cluster0-idqso.mongodb.net/b43_merng_db?retryWrites=true&w=majority", {
// 	useNewUrlParser:true
// });

mongoose.connect(
	"mongodb+srv://dbAdmin:mc1027z10@777@nosqlsession-n1wcz.mongodb.net/capstone3?retryWrites=true&w=majority",
	{
		useCreateIndex: true,
		useNewUrlParser: true
	}
);

// Console.log to know if you are connected to mongoDB
mongoose.connection.once("open", () => {
	console.log("Connected to mongoDB.");
});

app.use(bodyParser.json({ limit: "15mb" }));

app.use("/images", express.static("images"));

// Initialize apollo server
const server = require("./queries/queries");

// Serve the app using apolloServer
server.applyMiddleware({
	app,
	path: "/capstone3"
});

//create a listener to log if you are connected

app.listen(4000, () => {
	console.log(
		`🚀  Server ready at http://localhost:4000${server.graphqlPath}`
	);
});

// this will not work if there is no "./queries/queries" directory.
// next step is to create it
