const { ApolloServer, gql } = require("apollo-server-express");
const { GraphQLDate } = require("graphql-iso-date");
const bcrypt = require("bcrypt");
const uuid = require("uuid/v1");
const fs = require("fs");

// mongoose models. Import your models here.
const User = require("../models/User");
const TeeTime = require("../models/TeeTime");
const Booking = require("../models/Booking");

// Better to Delete all contents and try to recreate it using your own data.
// Just follow the syntax
const customScalarResolver = {
	Date: GraphQLDate
};

const typeDefs = gql`
	# this is a comment
	# the type query is the root of all GraphQL queries
	# this is used for executing GET requests

	#define your schemas

	#To make this work you need to create Models and import it above.

	scalar Date

	type UserType {
		id: ID
		firstName: String!
		lastName: String!
		role: String!
		password: String!
		username: String!
		email: String!
		imageLocation: String
		createdAt: Date
		updatedAt: Date
		bookings: BookingType
	}

	type TeeTimeType {
		id: ID
		date: Date!
		price: String!
		time: String!
		holes: String!
		players: String!
	}

	type BookingType {
		id: ID
		username: String!
		teeTimeId: String!
		status: String!
		players: String!
		users: UserType
		teetimes: TeeTimeType
	}

	type StatusType {
		id: ID
		name: String!
	}

	#define our CRUD
	#R - Retrieve. SYNTAX:

	type Query {
		getUsers: [UserType]
		getTeeTimes: [TeeTimeType]
		getBookings: [BookingType]
		getStatuses: [StatusType]
		getPendingStatus: BookingType

		getUser(id: ID!, name: String): UserType
		getTeeTime(id: ID!): TeeTimeType
		getBooking(id: ID!): BookingType
		getStatus(id: ID!): StatusType
	}

	#C - Create, U - update, D - Delete. SYNTAX:

	type Mutation {
		#Sample for create mutation:
		createUser(
			firstName: String!
			lastName: String!
			role: String!
			username: String!
			password: String!
			email: String!
			imageLocation: String
		): UserType

		createTeeTime(
			date: Date!
			price: String!
			time: String!
			holes: String!
			players: String!
		): TeeTimeType

		createBooking(
			username: String!
			teeTimeId: String!
			status: String!
			players: String!
		): BookingType

		#Sample for update mutation:
		updateUser(
			id: ID!
			firstName: String!
			lastName: String!
			role: String!
			username: String!
			password: String!
			email: String!
			imageLocation: String
		): UserType

		updateTeeTime(
			id: ID!
			date: Date!
			price: String!
			time: String!
			holes: String!
			players: String!
		): TeeTimeType

		updateBooking(
			id: ID!
			username: String!
			teeTimeId: String!
			status: String!
			players: String!
		): BookingType

		#Sample for deleting:
		deleteUser(id: ID!): Boolean
		deleteTeeTime(id: ID!): Boolean
		deleteBooking(id: ID!): Boolean

		#Sample for custom mutations:
		logInUser(username: String!, password: String!): UserType
	}
`;

// Sample resolver
const resolvers = {
	// resolver are what are we going to return when the query is executed

	// Include queries inside Query
	Query: {
		// Sample Query
		getUsers: () => {
			return User.find({});
		},
		getTeeTimes: () => {
			return TeeTime.find({});
		},
		getBookings: () => {
			return Booking.find({});
		},
		getPendingStatus: () => {
			return Booking.find({ status: "approved" });
		},

		getUser: (_, args) => {
			return User.findById(args.id);
		},
		getTeeTime: (_, args) => {
			return TeeTime.findById(args.id);
		},
		getBooking: (_, args) => {
			return Booking.findById(args.id);
		}
	},

	// Include mutations inside Mutation
	Mutation: {
		// Sample Mutation
		createUser: (_, args) => {
			let imageString = args.imageLocation;
			let imageBase = imageString.split(";base64").pop();
			let imageLocation = "images/" + uuid() + ".png";

			fs.writeFile(
				imageLocation,
				imageBase,
				{ encoding: "base64" },
				err => {}
			);

			let newUser = new User({
				firstName: args.firstName,
				lastName: args.lastName,
				role: args.role,
				email: args.email,

				//Syntax: bcrypt(plain password, salt rounds)
				password: bcrypt.hashSync(args.password, 10),
				username: args.username,
				imageLocation: imageLocation
			});

			console.log("Creating a user...");
			console.log(args);
			return newUser.save();
		},

		createTeeTime: (_, args) => {
			let newTeeTime = new TeeTime({
				date: args.date,
				price: args.price,
				time: args.time,
				holes: args.holes,
				players: args.players
			});

			console.log("Creating a tee time...");
			console.log(args);
			return newTeeTime.save();
		},

		createBooking: (_, args) => {
			let newBooking = new Booking({
				username: args.username,
				teeTimeId: args.teeTimeId,
				status: args.status,
				players: args.players
			});

			console.log("Creating a new booking...");
			console.log(args);
			return newBooking.save();
		},

		updateUser: (_, args) => {
			console.log("Updating user info...");
			console.log(args);
			let imageString = args.imageLocation;
			let imageBase = imageString.split(";base64").pop();
			let imageLocation = "images/" + uuid() + ".png";

			fs.writeFile(
				imageLocation,
				imageBase,
				{ encoding: "base64" },
				err => {}
			);

			let condition = { _id: args.id };
			let updates = {
				firstName: args.firstName,
				lastName: args.lastName,
				role: args.role,
				email: args.email,
				password: args.password,
				username: args.username,
				imageLocation: imageLocation
			};

			return User.findOneAndUpdate(condition, updates);
		},

		updateTeeTime: (_, args) => {
			console.log("Updating teetime info...");
			console.log(args);

			let condition = { _id: args.id };
			let updates = {
				date: args.date,
				price: args.price,
				time: args.time,
				holes: args.holes,
				players: args.players
			};

			return TeeTime.findOneAndUpdate(condition, updates);
		},

		updateBooking: (_, args) => {
			console.log("Updating booking info...");
			console.log(args);

			let condition = { _id: args.id };
			let updates = {
				username: args.username,
				teeTimeId: args.teeTimeId,
				status: args.status,
				players: args.players
			};

			return Booking.findOneAndUpdate(condition, updates);
		},

		deleteUser: (_, args) => {
			console.log("Deleting User...");
			console.log(args);

			return User.findByIdAndDelete(args.id).then((user, err) => {
				console.log(err);
				console.log(user);
				if (err || !user) {
					console.log("delete failed.");
					return false;
				}

				console.log("User deleted");
				return true;
			});
		},

		deleteTeeTime: (_, args) => {
			console.log("Deleting User...");
			console.log(args);

			return TeeTime.findByIdAndDelete(args.id).then((teetime, err) => {
				console.log(err);
				console.log(teetime);
				if (err || !teetime) {
					console.log("delete failed.");
					return false;
				}

				console.log("Tee time deleted");
				return true;
			});
		},

		deleteBooking: (_, args) => {
			console.log("Deleting booking...");
			console.log(args);

			return Booking.findByIdAndDelete(args.id).then((booking, err) => {
				console.log(err);
				console.log(booking);
				if (err || !booking) {
					console.log("delete failed.");
					return false;
				}

				console.log("Booking deleted");
				return true;
			});
		},

		logInUser: (_, args) => {
			console.log("trying to log in...");
			console.log(args);

			// returns the member in our members collection
			// with the same value as args.firstName
			return User.findOne({ username: args.username }).then(user => {
				console.log(user);

				if (user === null) {
					console.log("member not found");
					return null;
				}

				console.log(user.password);
				console.log(args.password);

				// compare the hashed version version of args.password
				// with member.password(already hashed)
				// syntax : bcrypt.compareSync(plain_pass, hashed_pass)

				// create a variable called hashedPassword and assign
				// the value of the result of bcrypt.compareSync
				let hashedPassword = bcrypt.compareSync(
					args.password,
					user.password
				);

				// console.log(hashedPassword);

				// if hashedPasword is false, output in the console wrong password
				if (!hashedPassword) {
					console.log("wrong password");
					return null;
				}
				// else output in the console login successful and  return member
				else {
					// successful login
					console.log(user);
					return user;
				}
			});
		}
	},

	UserType: {
		bookings: (parent, args) => {
			console.log(parent);
			return Booking.findOne({ username: parent.username });
			// return Booking.find({ userId: parent.id });
		}
	},

	// custom resolvers. (you can have many)
	BookingType: {
		users: (parent, args) => {
			return User.findOne({ username: parent.username });
		},
		teetimes: (parent, args) => {
			console.log(parent);
			return TeeTime.findOne({ _id: parent.teeTimeId });
		}
	}

	// MemberType : {
	// Sample resolver
	// team : (parent, args) => {
	// 	console.log(parent)
	// 	return Team.findById(parent.teamID)
	// }
	// }
};

// Pass the typedefs and resolvers to a variable
// This will be used to communicate with App.js
const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;
